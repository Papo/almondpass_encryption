#!/bin/sh
MESON_ACTION=${1:-'compile'}

VERSION=1.6.2
SOVERSION="$(echo $VERSION | cut -f 1-2 -d '.')"

printf "option('version', type : 'string', value : '%s')\noption('soversion', type : 'string', value : '%s')\n" "$VERSION" "$SOVERSION" > meson_options.txt

docker kill almond_encryption_android
docker rm almond_encryption_android
docker build --file android_build.Dockerfile -t almond_encryption_android .
docker run \
    -v $(pwd)/src:/build/src/ \
    -v $(pwd)/android_build:/build/target \
    --env MESON_ACTION"=$MESON_ACTION" \
    --env VERSION"=$VERSION" \
    --env SOVERSION"=$SOVERSION" \
    --name=almond_encryption_android \
    almond_encryption_android
