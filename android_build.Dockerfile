FROM debian:bullseye-slim

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y curl unzip clang meson pkg-config apt-utils
RUN curl -sSL https://deb.nodesource.com/setup_18.x | bash -
RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y nodejs openjdk-11-jdk python

ENV ANDROID_SDK_ROOT=/build/sdk/
ENV PATH="$PATH:$ANDROID_SDK_ROOT/emulator:$ANDROID_SDK_ROOT/platform-tools"

WORKDIR /build/sdk/cmdline-tools

RUN curl -sSL https://dl.google.com/android/repository/commandlinetools-linux-8512546_latest.zip > android_cl.zip && unzip android_cl.zip && mv cmdline-tools latest
RUN yes | ./latest/bin/sdkmanager ndk-bundle 'platforms;android-30' 'build-tools;30.0.3' 'cmake;3.18.1' 'sources;android-30' 'system-images;android-30;default;arm64-v8a' 'system-images;android-30;google_apis;x86_64'

WORKDIR /build

RUN curl -sSL https://download.libsodium.org/libsodium/releases/libsodium-1.0.18.tar.gz > libsodium.tar.gz && tar -xf libsodium.tar.gz && rm libsodium.tar.gz

ENV ANDROID_NDK_HOME="$ANDROID_SDK_ROOT/ndk-bundle"

WORKDIR /build/libsodium-1.0.18
RUN mkdir -p /build/pkg_config_dirs/aarch64-linux-android/ && mkdir -p /build/pkg_config_dirs/armv7a-linux-android/ && mkdir -p /build/pkg_config_dirs/x86_64-linux-android/ && mkdir -p /build/pkg_config_dirs/i686-linux-android/
RUN ./dist-build/android-armv8-a.sh && cp libsodium.pc /build/pkg_config_dirs/aarch64-linux-android/
RUN ./dist-build/android-armv7-a.sh && cp libsodium.pc /build/pkg_config_dirs/armv7a-linux-android/
RUN ./dist-build/android-x86_64.sh && cp libsodium.pc /build/pkg_config_dirs/x86_64-linux-android/
RUN ./dist-build/android-x86.sh && cp libsodium.pc /build/pkg_config_dirs/i686-linux-android/

WORKDIR /build/
COPY meson.build .
COPY crossfiles/ crossfiles
COPY meson_options.txt .
COPY build_scripts/in_container_android_build.sh .
COPY LICENCE LICENCE

ENV MESON_ACTION="compile"

ENTRYPOINT ./in_container_android_build.sh
