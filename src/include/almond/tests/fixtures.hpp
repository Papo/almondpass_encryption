#include <string>

#include "../data/data.hpp"

using namespace almond_data;


#ifndef ALMOND_FIXTURES
#define ALMOND_FIXTURES

int some_positive_int(int maximum = 0) {
	int some_int = rand();

	if (maximum > 0) {
		some_int = some_int % maximum + 1;
	}

	return some_int;
}

std::string some_string() {
	std::string s;
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";

	for (int i = 0; i < 20; ++i) {
		s.push_back(alphanum[some_positive_int(sizeof(alphanum) - 1) - 1]);
	}

	return s;
}


DataEntry some_entry(bool add_properties=true) {
	DataEntry entry;

	entry.name = some_string();

	if (add_properties) {
		for (int i = 0; i < some_positive_int(5) + 5; i++) {
			entry.properties[some_string()] = some_string();
		}
	}

	return entry;
};


Database some_database(bool add_entries=true) {
	Database db;
	db.name = some_string();
	if (add_entries) {
		for (int i = 0; i < 8; i++) {
			db.add_data_entry(some_entry());
		}
	}

	return db;
}

#endif
