#include <string>
#include "../encryption/encryption.hpp"
#include "local.hpp"

#ifndef ALMOND_ENCRYPTED_STORAGE
#define ALMOND_ENCRYPTED_STORAGE

namespace almond_storage {
	struct EncryptedStorage {
		LocalStorage storage;

		EncryptedStorage(const char* path_param);
		EncryptedStorage(const std::string path_param);

		std::string read_key_salt();
		std::string read(almond_enc::Key);
		almond_enc::EncryptedMessage read();
		void write(std::string, almond_enc::Key);
		void write(almond_enc::EncryptedMessage message, std::string key_salt);
	};
};

#endif
