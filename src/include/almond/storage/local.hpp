#include <string>

#ifndef ALMOND_LOCAL_STORAGE
#define ALMOND_LOCAL_STORAGE

#define FILEFORMAT_VERSION_WIDTH 7
#define FILEFORMAT_VERSION "APDB001"

namespace almond_storage {
	struct LocalStorage {
		std::string path;

		LocalStorage(const char* path_param);
		LocalStorage(const std::string path_param);

		std::string read_file_version();
		std::string read(int offset=0);
		void write(std::string);

		std::string get_temp_path();
	};
}

#endif
