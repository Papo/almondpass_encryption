#include <chrono>
#include <iomanip>
#include <ctime>
#include <string>
#include <sstream>


#ifndef ALMOND_UTILS
#define ALMOND_UTILS

namespace almond_data {
	std::string _strftime(std::chrono::time_point<std::chrono::system_clock> time_, const char* fmt);
	std::string serialize(std::chrono::time_point<std::chrono::system_clock> time_);
	std::chrono::time_point<std::chrono::system_clock> parse_time(std::string seconds_since_epoch);
}

#endif
