#include "../errors.hpp"
#include <cstdio>

#ifndef ALMOND_DATA_ERRORS
#define ALMOND_DATA_ERRORS

namespace almond_data {
	struct EncryptedDBError : SimpleTemplateException {};

	struct EntryAlreadyExists : EncryptedDBError {
		const std::string error_code = std::string("entry_already_exists");
		const std::string message_template = std::string(
			"An entry with name \"%s\" already exists in database \"%s\"."
		);

		EntryAlreadyExists(std::string entry_name, std::string db_name) {
			this->error_message = compile_error_template(
				this->message_template.c_str(),
				entry_name.c_str(),
				db_name.c_str()
			);
		}
	};

	struct EntryAlreadyHasProperty : EncryptedDBError {
		const std::string error_code = std::string("entry_already_has_property");
		const std::string message_template = std::string(
			"A property with name \"%s\" already exists in entity \"%s\"."
		);

		EntryAlreadyHasProperty(std::string property, std::string entry_name) {
			this->error_message = compile_error_template(
				this->message_template.c_str(),
				property.c_str(),
				entry_name.c_str()
			);
		}
	};

	struct EntryDoesNotExist : EncryptedDBError {
		const std::string error_code = std::string("entry_does_not_exist");
		const std::string message_template = std::string(
			"Could not find an entry with name \"%s\" in database \"%s\"."
		);

		EntryDoesNotExist(std::string entry_name, std::string db_name) {
			this->error_message = compile_error_template(
				this->message_template.c_str(),
				entry_name.c_str(),
				db_name.c_str()
			);
		}
	};

	struct PropertyDoesNotExist : EncryptedDBError {
		const std::string error_code = std::string("property_does_not_exist");
		const std::string message_template = std::string(
			"Could not find a property \"%s\" in entry \"%s\"."
		);

		PropertyDoesNotExist(std::string property, std::string entry_name) {
			this->error_message = compile_error_template(
				this->message_template.c_str(),
				property.c_str(),
				entry_name.c_str()
			);
		}
	};

	struct DatabaseLoadError : EncryptedDBError {
		const std::string error_code = std::string("database_load_error");
		const std::string error_message = std::string("Could not load database.");
	};
}

#endif
