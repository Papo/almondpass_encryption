#include <chrono>
#include <string>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <unordered_set>
#include <utility>
#include "../json11/json11.hpp"
#include "../storage/encrypted_storage.hpp"
#include "errors.hpp"
#include "utils.hpp"

# ifndef ALMOND_DATA_MODULE
# define ALMOND_DATA_MODULE


namespace almond_data {
	struct DataEntry {
		std::string name;
		std::map<std::string, std::string> properties;
		std::chrono::time_point<std::chrono::system_clock> edited;

		DataEntry() = default;
		DataEntry(const std::string name, const std::map<std::string, std::string> properties)
			: name(name), properties(properties) {}
		DataEntry(
			const std::string name,
			const std::chrono::time_point<std::chrono::system_clock> edited,
			const std::map<std::string, std::string> properties
		) : name(name), properties(properties), edited(edited) {}
		DataEntry(json11::Json parsed);
		json11::Json to_json() const;

		void add_property(std::string name);
		void add_property(std::string name, std::string value);
		void set_property(std::string name, std::string value);
		std::vector<std::string> get_property_names();
		std::string get_property_value(std::string name);
		void remove_property(std::string name);
		void update_edited();
	};

	bool operator==(const DataEntry& lho, const DataEntry& rho);
	bool operator!=(const DataEntry& lho, const DataEntry& rho);

	enum LOG_ACTION {
		ADD,
		REMOVE,
		UPDATE
	};

	std::string serialize(const LOG_ACTION action);
	LOG_ACTION parse_log_action(const std::string serialized);

	struct LogLine {
		std::string id;
		std::string parent_id;
		std::chrono::time_point<std::chrono::system_clock> t;
		LOG_ACTION action;
		std::string entry_name;
		DataEntry entry;

		LogLine(
			std::string parent_id,
			LOG_ACTION action,
			std::string entry_name,
			DataEntry entry
		) :
			parent_id(parent_id),
			action(action),
			entry_name(entry_name),
			entry(entry)
		{
			this->t = std::chrono::system_clock::now();

			std::stringstream id_stream;
			for (int i = 0; i < 4; i++) {
				auto id_part = randombytes_random();
				std::stringstream part_stream;
				part_stream << std::setfill('0') << std::setw(8) << std::setbase(16);
				part_stream << id_part;
				id_stream << part_stream.str();
			}
			this->id = id_stream.str();
		}

		LogLine(json11::Json parsed);

		json11::Json to_json() const;
	};

	bool operator==(const LogLine& lho, const LogLine& rho);
	bool operator!=(const LogLine& lho, const LogLine& rho);

	struct Conflict {
		std::string entry_name;
		std::vector<LogLine> left;
		std::vector<LogLine> right;
	};

	struct Diff {
		std::vector<LogLine> common;
		std::vector<LogLine> left;
		std::vector<LogLine> right;
		std::vector<Conflict> conflicts;
	};

	struct Database {
		std::string name;
		std::vector<LogLine> log;
		std::map<std::string, DataEntry> entries;
		std::chrono::time_point<std::chrono::system_clock> edited;
		std::chrono::time_point<std::chrono::system_clock> saved;
		std::chrono::time_point<std::chrono::system_clock> instantiated;

		Database() {
			this->instantiated = std::chrono::system_clock::now();
		}
		Database(
			const std::string name, const std::map<std::string, DataEntry> entries
		) : name(name), entries(entries) {
			this->instantiated = std::chrono::system_clock::now();
		}
		Database(
			const std::string name,
			const std::chrono::time_point<std::chrono::system_clock> edited,
			const std::map<std::string, DataEntry> entries
		) : name(name), entries(entries), edited(edited) {
			this->instantiated = std::chrono::system_clock::now();
		}

		std::string serialize() const;
		std::vector<std::string> get_entry_keys();

		void add_data_entry(DataEntry entry);
		void write(almond_storage::EncryptedStorage storage, almond_enc::Key);
		void collapse_log();

		almond_data::DataEntry get_entry(std::string name);
		void update_entry(DataEntry entry);
		std::vector<std::string> search_name(std::string name);
		void remove_data_entry(std::string name);

		void update_edited();
		void append_log(std::vector<LogLine> log);
		std::chrono::time_point<std::chrono::system_clock> get_last_edited();
		bool is_unsaved();
	};

	bool operator==(const Database& lho, const Database& rho);
	bool operator!=(const Database& lho, const Database& rho);

	Database load(const std::string& serialized);
	Database read(almond_storage::EncryptedStorage, almond_enc::Key);

	Diff diff(const Database& lho, const Database& rho);
}

#endif
