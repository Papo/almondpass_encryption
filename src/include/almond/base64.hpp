#include <string>
#include <sstream>
#include <stdexcept>

#ifndef ALMOND_BASE64_H
#define ALMOND_BASE64_H

namespace almond_base_64 {
    const static char BASE64_PAD = '=';

    class Base64DecodeError : public std::runtime_error {
    public:
        Base64DecodeError(const std::string& what_arg);
    };
    std::string decode(std::string encoded);
    std::string encode(std::string decoded);
};

#endif
