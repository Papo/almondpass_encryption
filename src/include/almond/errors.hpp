#include <string>
#include <cstdio>
#include <cstdarg>

#ifndef ALMOND_BASE_ERRORS
#define ALMOND_BASE_ERRORS

const int MAX_ERROR_SIZE = 1024 * 8;


struct BaseException {
	std::string error_code;
	std::string error_message;
};


struct SimpleTemplateException {
	std::string error_code;
	std::string error_message;
	std::string message_template;
};


std::string compile_error_template(const char* message_template, ...);

#endif
