#include <string>
#include <vector>
#include <sodium.h>

#define XCHACHA20_NONCE_BYTES crypto_aead_xchacha20poly1305_ietf_NPUBBYTES
#define XCHACHA20_KEY_BYTES crypto_aead_xchacha20poly1305_ietf_KEYBYTES
#define XCHACHA20_AUTH_BYTES crypto_aead_xchacha20poly1305_ietf_ABYTES

# ifndef ALMOND_ENCRYPTION_MODULE
# define ALMOND_ENCRYPTION_MODULE


namespace almond_enc {
	struct EncryptedMessage {
		std::string ciphertext;
		std::string nonce;

		EncryptedMessage();
		EncryptedMessage(std::string pciphertext, std::string pnonce);

		EncryptedMessage& operator=(const EncryptedMessage other);

		bool is_empty();
	};

	struct Key {
		std::string bytes;
		std::string salt;
		Key(){}
		Key(std::string pbytes, std::string salt): bytes(pbytes), salt(salt) {}

		bool is_empty();
	};

	struct Buffer {
		public:
			Buffer(unsigned long long max_length);
			~Buffer();

			unsigned char* buffer;
			unsigned long long length;
			unsigned long long max_length;
	};

	std::string message_to_string(
		EncryptedMessage crypto_aead_chacha20poly1305_ietf_messagebytes_max
	);

	EncryptedMessage message_from_string(std::string);

	EncryptedMessage encrypt_message(std::string message, Key key);

	std::string decrypt_message(EncryptedMessage encrypted_message, Key key);

	Key derive_key(std::string passphrase, std::string salt);

	std::string generate_key_salt();

	std::string generate_password(
		unsigned int size, std::vector<char> population
	);
}

# endif
