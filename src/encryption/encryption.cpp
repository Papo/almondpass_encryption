#include <sodium.h>
#include "../include/almond/encryption/encryption.hpp"

using namespace std;
using namespace almond_enc;

almond_enc::Buffer::Buffer(unsigned long long max_length) {
	this->buffer = new unsigned char[max_length];
	this->max_length = max_length;
	this->length = 0;
}

almond_enc::Buffer::~Buffer() {
	delete[] this->buffer;
}


bool almond_enc::Key::is_empty() {
	return this->salt.size() + this->bytes.size() == 0;
}

almond_enc::EncryptedMessage::EncryptedMessage() {}

bool almond_enc::EncryptedMessage::is_empty() {
	return this->nonce.size() + this->ciphertext.size() == 0;
}

almond_enc::EncryptedMessage::EncryptedMessage(string pciphertext, string pnonce) {
	this->ciphertext = pciphertext;
	this->nonce = pnonce;
}


EncryptedMessage almond_enc::encrypt_message(
	string message,
	Key key
) {
	Buffer ciphertext(message.length() + XCHACHA20_AUTH_BYTES);

	unsigned char nonce[XCHACHA20_NONCE_BYTES];
	randombytes_buf(nonce, sizeof nonce);

	crypto_aead_xchacha20poly1305_ietf_encrypt(
		ciphertext.buffer,
		&ciphertext.length,
		(unsigned char*)message.data(),
		message.length(),
		NULL,
		0,
		NULL,
		nonce,
		(unsigned char*)key.bytes.data()
	);

	return EncryptedMessage(
		string((char*)ciphertext.buffer, ciphertext.length),
		string((char*)nonce, sizeof nonce)
	);
}


string almond_enc::decrypt_message(EncryptedMessage encrypted, Key key) {
	Buffer decrypted(encrypted.ciphertext.length() - XCHACHA20_AUTH_BYTES);

	int err = crypto_aead_xchacha20poly1305_ietf_decrypt(
		decrypted.buffer,
		&decrypted.length,
		NULL,
		(unsigned char*)encrypted.ciphertext.data(),
		encrypted.ciphertext.length(),
		NULL,
		0,
		(unsigned char*)encrypted.nonce.data(),
		(unsigned char*)key.bytes.data()
	);

	return string((char*)decrypted.buffer, decrypted.length);
}


Key almond_enc::derive_key(string passphrase, string salt) {
	unsigned char key[XCHACHA20_KEY_BYTES];

	crypto_pwhash(
		key,
		XCHACHA20_KEY_BYTES,
		passphrase.data(),
		passphrase.length(),
		(unsigned char*)salt.data(),
		crypto_pwhash_OPSLIMIT_INTERACTIVE,
		crypto_pwhash_MEMLIMIT_INTERACTIVE,
		crypto_pwhash_ALG_DEFAULT
	);

	return Key(string((char*)key, sizeof key), salt);
}


string almond_enc::generate_key_salt() {
	unsigned char salt[crypto_pwhash_SALTBYTES];

	randombytes_buf(salt, sizeof salt);

	return string((char*)salt, sizeof salt);
}


string almond_enc::message_to_string(EncryptedMessage message) {
	return message.nonce + message.ciphertext;
}


string almond_enc::generate_password(
	unsigned int size, vector<char> population
) {
	if (population.size() == 0 || size == 0) {
		return string();
	}

	vector<unsigned int> buffer(size);
	randombytes_buf(buffer.data(), buffer.size() * sizeof(unsigned int));

	vector<char> sample;
	for (auto choice : buffer) {
		sample.push_back(population[choice % population.size()]);
	}

	return string(sample.begin(), sample.end());
}

EncryptedMessage almond_enc::message_from_string(string message) {
	return EncryptedMessage(
		message.substr(XCHACHA20_NONCE_BYTES, message.length()),
		message.substr(0, XCHACHA20_NONCE_BYTES)
	);
}


EncryptedMessage& EncryptedMessage::operator=(const EncryptedMessage other) {
	this->ciphertext = other.ciphertext;
	this->nonce = other.nonce;

	return *this;
}
