#include "include/almond/base64.hpp"

almond_base_64::Base64DecodeError::Base64DecodeError(const std::string& what_arg) : std::runtime_error(what_arg) {}


static const char table_a2b_base64[] = {
    64,64,64,64, 64,64,64,64, 64,64,64,64, 64,64,64,64,
    64,64,64,64, 64,64,64,64, 64,64,64,64, 64,64,64,64,
    64,64,64,64, 64,64,64,64, 64,64,64,62, 64,64,64,63,
    52,53,54,55, 56,57,58,59, 60,61,64,64, 64, 0,64,64, /* Note PAD->0 */
    64, 0, 1, 2,  3, 4, 5, 6,  7, 8, 9,10, 11,12,13,14,
    15,16,17,18, 19,20,21,22, 23,24,25,64, 64,64,64,64,
    64,26,27,28, 29,30,31,32, 33,34,35,36, 37,38,39,40,
    41,42,43,44, 45,46,47,48, 49,50,51,64, 64,64,64,64,

    64,64,64,64, 64,64,64,64, 64,64,64,64, 64,64,64,64,
    64,64,64,64, 64,64,64,64, 64,64,64,64, 64,64,64,64,
    64,64,64,64, 64,64,64,64, 64,64,64,64, 64,64,64,64,
    64,64,64,64, 64,64,64,64, 64,64,64,64, 64,64,64,64,
    64,64,64,64, 64,64,64,64, 64,64,64,64, 64,64,64,64,
    64,64,64,64, 64,64,64,64, 64,64,64,64, 64,64,64,64,
    64,64,64,64, 64,64,64,64, 64,64,64,64, 64,64,64,64,
    64,64,64,64, 64,64,64,64, 64,64,64,64, 64,64,64,64,
};

static const char table_b2a_base64[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";



std::string almond_base_64::decode(std::string encoded){
    if (encoded.length() == 0) {
        return std::string();
    }

    const unsigned char *ascii_data = (unsigned char*)encoded.data();
    size_t ascii_len = encoded.length();
    char padding_started = 0;
    std::string bin_data;

    if (ascii_len > 0 && ascii_data[0] == '=') {
        throw Base64DecodeError("Leading padding not allowed");
    }

    int quad_pos = 0;
    unsigned char leftchar = 0;
    int pads = 0;
    for (size_t i = 0; i < ascii_len; i++) {
        unsigned char this_ch = ascii_data[i];

        /* Check for pad sequences and ignore
        ** the invalid ones.
        */
        if (this_ch == BASE64_PAD) {
            padding_started = 1;

            if (quad_pos >= 2 && quad_pos + ++pads >= 4) {
                /* A pad sequence means we should not parse more input. */
                if (i + 1 < ascii_len) {
                    throw Base64DecodeError("Excess data after padding");
                }

                return bin_data;
            }
            continue;
        }

        this_ch = table_a2b_base64[this_ch];
        if (this_ch >= 64) {
            throw Base64DecodeError("Only base64 data is allowed");
        }

        // Characters that are not '=', in the middle of the padding, are not allowed
        if (padding_started) {
            throw Base64DecodeError("Discontinuous padding not allowed");
        }
        pads = 0;

        switch (quad_pos) {
            case 0:
                quad_pos = 1;
                leftchar = this_ch;
                break;
            case 1:
                quad_pos = 2;
                bin_data.push_back((leftchar << 2) | (this_ch >> 4));
                leftchar = this_ch & 0x0f;
                break;
            case 2:
                quad_pos = 3;
                bin_data.push_back((leftchar << 4) | (this_ch >> 2));
                leftchar = this_ch & 0x03;
                break;
            case 3:
                quad_pos = 0;
                bin_data.push_back((leftchar << 6) | (this_ch));
                leftchar = 0;
                break;
        }
    }

    if (quad_pos != 0) {
        if (quad_pos == 1) {
            /*
            ** There is exactly one extra valid, non-padding, base64 character.
            ** This is an invalid length, as there is no possible input that
            ** could encoded into such a base64 string.
            */
            std::stringstream error_message;
            error_message << "Invalid base64-encoded string: number of data characters ";
            error_message << "(" << (bin_data.length()) / 3 * 4 + 1 << ") ";
            error_message << "cannot be 1 more than a multiple of 4";
            throw Base64DecodeError(error_message.str());
        } else {
            throw Base64DecodeError("Incorrect padding");
        }
    }

    return bin_data;
}


std::string almond_base_64::encode(std::string decoded) {
    std::string ascii_data;

    int leftbits = 0;
    unsigned char this_ch;
    unsigned int leftchar = 0;
    size_t bin_len;

    const unsigned char *bin_data = (unsigned char*) decoded.data();
    bin_len = decoded.length();
    if (bin_len == 0) {
        return ascii_data;
    }

    for( ; bin_len > 0 ; bin_len--, bin_data++ ) {
        /* Shift the data into our buffer */
        leftchar = (leftchar << 8) | *bin_data;
        leftbits += 8;

        /* See if there are 6-bit groups ready */
        while ( leftbits >= 6 ) {
            this_ch = (leftchar >> (leftbits-6)) & 0x3f;
            leftbits -= 6;
            ascii_data.push_back(table_b2a_base64[this_ch]);
        }
    }
    if ( leftbits == 2 ) {
        ascii_data.push_back(table_b2a_base64[(leftchar&3) << 4]);
        ascii_data.push_back(BASE64_PAD);
        ascii_data.push_back(BASE64_PAD);
    } else if ( leftbits == 4 ) {
        ascii_data.push_back(table_b2a_base64[(leftchar&0xf) << 2]);
        ascii_data.push_back(BASE64_PAD);
    }

    return ascii_data;
}
