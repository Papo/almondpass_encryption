#include <chrono>

#include "../include/almond/include_test/catch.hpp"
#include "../include/almond/data/utils.hpp"
#include <ctime>

using namespace std;
using namespace chrono;

TEST_CASE("Test time serialization") {
    time_point<system_clock> point_in_time;

    auto serialized = almond_data::serialize(point_in_time);

    CHECK(serialized == string("0"));

    auto parsed = almond_data::parse_time(serialized);

    CHECK(point_in_time == parsed);
}


TEST_CASE("Test deserialization") {
    string time_stamp("24500000");
    auto parsed = almond_data::parse_time(time_stamp);
    CHECK(almond_data::serialize(parsed) == time_stamp);
}
