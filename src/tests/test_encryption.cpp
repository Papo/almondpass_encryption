#include <sodium.h>
#include <cstring>
#include <unordered_set>

#include "../include/almond/include_test/catch.hpp"
#include "../include/almond/encryption/encryption.hpp"

using namespace std;


TEST_CASE("Encrypt encrypts and decrypt decrypts") {
	sodium_init();

	string message("Some message.");
	string password("Correct Battery Horse Staple!");

	SECTION("generate_key_salt is not throwing errors.") {
		string salt = almond_enc::generate_key_salt();
		CHECK(salt.length() == crypto_pwhash_SALTBYTES);
	}

	SECTION("derive_key produces a key of the correct size.") {
		string salt = almond_enc::generate_key_salt();
		almond_enc::Key key = almond_enc::derive_key(password, salt);

		CHECK(key.bytes.length() == XCHACHA20_KEY_BYTES);
	}

	SECTION("derive_key produces consistent output") {
		string salt = almond_enc::generate_key_salt();

		almond_enc::Key derived_key_t1 = almond_enc::derive_key(password, salt);
		almond_enc::Key derived_key_t2 = almond_enc::derive_key(password, salt);
		CHECK(derived_key_t1.bytes == derived_key_t2.bytes);
	}

	unsigned char key_str[XCHACHA20_KEY_BYTES];
	randombytes_buf(key_str, sizeof key_str);
	almond_enc::Key some_random_key = almond_enc::derive_key(
		string((char*)key_str, sizeof key_str),
		almond_enc::generate_key_salt()
	);

	SECTION("encryption is not throwing errors.") {
		almond_enc::EncryptedMessage encrypted = almond_enc::encrypt_message(
			message, some_random_key
		);

		CHECK(encrypted.ciphertext != message);
		CHECK(
			encrypted.ciphertext.length()
			>= message.length() + XCHACHA20_AUTH_BYTES
		);
	}

	SECTION("decryption produces the original message.") {
		almond_enc::EncryptedMessage encrypted = almond_enc::encrypt_message(
			message, some_random_key
		);
		string decrypted = almond_enc::decrypt_message(encrypted, some_random_key);

		CHECK(decrypted == message);
	}

	SECTION("Encrypt and decrypt with a passphrase.") {
		string salt = almond_enc::generate_key_salt();
		almond_enc::Key key = almond_enc::derive_key(password, salt);
		almond_enc::EncryptedMessage encrypted = almond_enc::encrypt_message(
			message, key
		);

		string decrypted = almond_enc::decrypt_message(encrypted, key);
		CHECK(decrypted == message);
	}
}


TEST_CASE("Generate random samples") {
	sodium_init();
	vector<char> population = {'a', 'b', 'c', 'd', 'e', '$', ';', '*'};
	SECTION("generated sample is of the correct size when size < population") {
		int size = 2;
		auto sample = almond_enc::generate_password(size, population);
		CHECK(sample.size() == size);
	}

	SECTION("generated sample is of the correct size when size > population") {
		int size = 128;
		auto sample = almond_enc::generate_password(size, population);
		CHECK(sample.size() == size);
	}

	SECTION("generated sample only contains characters from the population") {
		int size = 128;
		auto sample = almond_enc::generate_password(size, population);

		unordered_set<char> population_set(
			population.begin(), population.end()
		);

		for (auto character : sample) {
			auto got = population_set.find(character);
			CHECK(got != population_set.end());
		}
	}

	SECTION("empty population returns an empty sample") {
		int size = 128;
		auto sample = almond_enc::generate_password(size, vector<char>());
		CHECK(sample.size() == 0);
	}

	SECTION("a size of zero returns an empty sample") {
		int size = 0;
		auto sample = almond_enc::generate_password(size, population);
		CHECK(sample.size() == 0);
	}

	SECTION("repeated calls do not return the same result") {
		int size = 128;
		auto first_sample = almond_enc::generate_password(size, population);
		auto second_sample = almond_enc::generate_password(size, population);
		CHECK(first_sample != second_sample);
	}
}
