#include "../include/almond/include_test/catch.hpp"
#include "../include/almond/storage/local.hpp"
#include <string>
#include <cstdio>


TEST_CASE("Local storage") {
	const char* tmp_file_name = "some_path.db";
	almond_storage::LocalStorage local_storage(tmp_file_name);

	std::string test_string("some_file_content");
	std::string expected_version(FILEFORMAT_VERSION);
	local_storage.write(test_string);

	CHECK(local_storage.read_file_version() == expected_version);
	CHECK(local_storage.read() == test_string);
	remove(tmp_file_name);
}
