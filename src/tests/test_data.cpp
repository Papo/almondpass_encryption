#include <algorithm>
#include <set>

#include "../include/almond/include_test/catch.hpp"
#include "../include/almond/encryption/encryption.hpp"
#include "../include/almond/data/data.hpp"
#include "../include/almond/tests/fixtures.hpp"

using namespace almond_data;
using namespace std;


TEST_CASE("DataEntry and Database serialiazation and deserialiazation") {
	DataEntry entry1;
	DataEntry entry2;

	entry1.name = string("some site");
	entry1.properties[string("username")] = string("some user");
	entry1.properties[string("password")] = string("some password");

	entry2.name = string("some other site");
	entry2.properties[string("username")] = string("some other user");
	entry2.properties[string("password")] = string(
		"some other password"
	);

	Database db1;

	db1.name = string("A database");
	db1.entries[entry1.name] = entry1;
	db1.entries[entry2.name] = entry2;
	LogLine log_line("", LOG_ACTION::ADD, entry1.name, entry1);
	log_line.id = string("ae9d4222326ed6932765ed8b27244e10");
	log_line.t = chrono::time_point<std::chrono::system_clock>();

	db1.log.push_back(log_line);

	CHECK(db1.get_entry_keys() == vector<string>({entry2.name, entry1.name}));

	string expected = ""
		"{\"edited\": \"0\", \"entries\": "
		"{\"some other site\": {\"edited\": \"0\", "
		"\"name\": \"some other site\", \"properties\": {\"password\": "
		"\"some other password\", \"username\": \"some other user\"}}, "
		"\"some site\": {\"edited\": \"0\", \"name\": "
		"\"some site\", \"properties\": {\"password\": \"some password\", "
		"\"username\": \"some user\"}}}, \"log\": [{\"action\": \"ADD\", "
		"\"entry\": {\"edited\": \"0\", \"name\": "
		"\"some site\", \"properties\": {\"password\": \"some password\", "
		"\"username\": \"some user\"}}, \"entry_name\": \"some site\", "
		"\"id\": \"ae9d4222326ed6932765ed8b27244e10\", \"parent_id\": \"\", "
		"\"t\": \"0\"}], \"name\": \"A database\"}";
	CHECK(db1.serialize() == expected);

	Database deserialized = load(expected);

	CHECK(deserialized == db1);
}


TEST_CASE("Add DataEntry to Database") {
	DataEntry entry1 = some_entry();
	Database db1 = some_database(false);
	auto before = chrono::system_clock::now();
	db1.add_data_entry(entry1);
	CHECK(db1.edited >= before);

	CHECK(db1.get_entry_keys() == vector<string>({entry1.name}));
	auto saved_entry = db1.entries[entry1.name];
	CHECK(saved_entry == entry1);
}


TEST_CASE("Add existing DataEntry to Database") {
	DataEntry entry1 = some_entry();
	Database db1 = some_database();
	db1.add_data_entry(entry1);

	CHECK_THROWS_AS(
		db1.add_data_entry(entry1),
		almond_data::EntryAlreadyExists
	);
}


TEST_CASE("Get entry from database") {
	DataEntry stored = some_entry();
	Database db1 = some_database();
	db1.add_data_entry(stored);

	DataEntry recovered = db1.get_entry(stored.name);
	CHECK(recovered == stored);
}


TEST_CASE("Get missing entry from database") {
	DataEntry stored = some_entry();
	Database db1 = some_database();
	db1.add_data_entry(stored);

	CHECK_THROWS_AS(
		db1.get_entry("Some key not in the database"),
		almond_data::EntryDoesNotExist
	);
}


TEST_CASE("Remove entry from database") {
	DataEntry stored = some_entry();
	Database db1 = some_database();
	db1.add_data_entry(stored);

	auto before = chrono::system_clock::now();
	db1.remove_data_entry(stored.name);
	CHECK(db1.edited >= before);
	CHECK(db1.entries.find(stored.name) == db1.entries.end());
	CHECK_THROWS_AS(
		db1.get_entry(stored.name),
		almond_data::EntryDoesNotExist
	);
}


TEST_CASE("Remove missing entry from database") {
	Database db1 = some_database();

	CHECK_THROWS_AS(
		db1.remove_data_entry("some key not in the database"),
		almond_data::EntryDoesNotExist
	);
}


TEST_CASE("Add empty property to entry") {
	DataEntry entry = some_entry();
	string prop_name = some_string();
	CHECK(entry.properties.find(prop_name) == entry.properties.end());
	auto before = chrono::system_clock::now();
	entry.add_property(prop_name);
	CHECK(entry.properties[prop_name] == string());
	CHECK(entry.edited >= before);
}


TEST_CASE("Add existing empty property to entry") {
	DataEntry entry = some_entry();
	string prop_name = some_string();
	entry.add_property(prop_name);
	CHECK_THROWS_AS(
		entry.add_property(prop_name),
		almond_data::EntryAlreadyHasProperty
	);
}


TEST_CASE("Add property with value to entry") {
	DataEntry entry = some_entry();
	string prop_name = some_string();
	string prop_value = some_string();

	auto before = chrono::system_clock::now();
	entry.add_property(prop_name, prop_value);
	CHECK(entry.properties[prop_name] == prop_value);
	CHECK(entry.edited >= before);
}


TEST_CASE("Add existing property with value to entry") {
	DataEntry entry = some_entry();
	string prop_name = some_string();
	entry.add_property(prop_name, some_string());

	CHECK_THROWS_AS(
		entry.add_property(prop_name),
		almond_data::EntryAlreadyHasProperty
	);
}


TEST_CASE("Set entry property value") {

	DataEntry entry = some_entry();
	string prop_name = some_string();
	string prop_value = some_string();

	auto before = chrono::system_clock::now();
	entry.set_property(prop_name, prop_value);
	CHECK(entry.properties[prop_name] == prop_value);
	CHECK(entry.edited >= before);

	string prop_other_value = some_string();
	entry.set_property(prop_name, prop_other_value);
	CHECK(entry.properties[prop_name] == prop_other_value);
}


TEST_CASE("Get property names (empty)") {
	DataEntry entry = some_entry(false);

	auto retrieved = entry.get_property_names();

	CHECK(retrieved.size() == 0);
}


TEST_CASE("Get property names") {
	DataEntry entry = some_entry(false);
	vector<string> prop_names;

	for (int i = 0; i < 0; i++) {
		string prop_name = some_string();
		prop_names.push_back(prop_name);
		entry.add_property(prop_name);
	}
	std::sort(prop_names.begin(), prop_names.end());

	auto retrieved = entry.get_property_names();
	std::sort(retrieved.begin(), retrieved.end());
	CHECK(retrieved == prop_names);
}


TEST_CASE("Remove property") {
	DataEntry entry = some_entry();
	string prop_name = some_string();
	entry.set_property(prop_name, some_string());
	auto before = chrono::system_clock::now();
	entry.remove_property(prop_name);
	CHECK(entry.properties.find(prop_name) == entry.properties.end());
	CHECK(entry.edited >= before);
}


TEST_CASE("Remove missing property") {
	DataEntry entry = some_entry();
	string prop_name = some_string();
	CHECK_THROWS_AS(
		entry.remove_property(prop_name),
		almond_data::PropertyDoesNotExist
	);
}


TEST_CASE("database get_last_edited takes entries into account") {
	Database db = some_database();
	DataEntry entry = some_entry();

	auto before = chrono::system_clock::now();

	entry.edited = chrono::system_clock::now();
	db.edited = chrono::system_clock::now();
	db.add_data_entry(entry);

	CHECK(db.edited > before);
	CHECK(db.get_last_edited() == db.edited);
	db.entries[entry.name].add_property(some_string());
	CHECK(db.get_last_edited() == db.entries[entry.name].edited);
}


TEST_CASE("database can be written and read from/to encrypted storage") {
	Database db = some_database();
	const char* tmp_file_name = "some_path_1.db";
	almond_storage::EncryptedStorage encrypted_storage(tmp_file_name);
	almond_enc::Key key = almond_enc::derive_key(
		some_string(),
		almond_enc::generate_key_salt()
	);

	db.write(encrypted_storage, key);
	Database read_db = read(encrypted_storage, key);

	CHECK(db == read_db);
}


TEST_CASE("database keeps track of its initialization time") {
	Database db = some_database();

	CHECK(db.instantiated > chrono::time_point<chrono::system_clock>());
}


TEST_CASE("Empty databases are unsaved") {
	Database db = some_database();

	CHECK(db.is_unsaved() == true);
}


TEST_CASE("Database saved database is not unsaved") {
	Database db = some_database();
	DataEntry entry = some_entry();
	db.add_data_entry(entry);

	almond_storage::EncryptedStorage encrypted_storage("some_path_2.db");
	almond_enc::Key key = almond_enc::derive_key(
		some_string(),
		almond_enc::generate_key_salt()
	);
	db.write(encrypted_storage, key);

	CHECK(db.is_unsaved() == false);
}


TEST_CASE("Database is unsaved when edited after saving") {
	Database db = some_database();
	db.add_data_entry(some_entry());

	almond_storage::EncryptedStorage encrypted_storage("some_path_3.db");
	almond_enc::Key key = almond_enc::derive_key(
		some_string(),
		almond_enc::generate_key_salt()
	);
	db.write(encrypted_storage, key);

	db.add_data_entry(some_entry());

	CHECK(db.is_unsaved() == true);
}


TEST_CASE("search_name returns full match") {
	Database db = some_database();
	DataEntry entry = some_entry();
	db.add_data_entry(entry);

	auto results = db.search_name(entry.name);

	CHECK(results[0] == entry.name);
}


TEST_CASE("search_name returns partial match") {
	Database db = some_database();
	DataEntry entry = some_entry();

	string prefix = some_string();
	entry.name = prefix + entry.name;
	db.add_data_entry(entry);

	auto results = db.search_name(prefix);

	CHECK(results[0] == entry.name);
}


TEST_CASE("search_name returns multiple results") {
	Database db = some_database();
	DataEntry entries[] = {
		some_entry(), some_entry(), some_entry(), some_entry()
	};

	string prefix = some_string();
	set<string> expected;
	for (auto entry: entries) {
		entry.name = prefix + entry.name;
		db.add_data_entry(entry);
		expected.insert(entry.name);
	}

	auto results = db.search_name(prefix);

	CHECK(results.size() == 4);
	CHECK(set<string>(results.begin(), results.end()) == expected);
}


TEST_CASE("search_name does not return non matching results") {
	Database db = some_database();
	DataEntry entry = some_entry();
	entry.name = string("a_string");
	db.add_data_entry(entry);

	string prefix = string("prefix_");
	auto results = db.search_name(prefix);
	CHECK(results.size() == 0);
}


TEST_CASE("update_entry updates entry") {
	Database db = some_database();
	DataEntry entry_a = some_entry();
	entry_a.name = string("a_string");
	db.add_data_entry(entry_a);

	DataEntry entry_b = some_entry();
	entry_b.name = entry_a.name;
	db.update_entry(entry_b);

	CHECK(db.entries[entry_a.name] == entry_b);
}


TEST_CASE("Test database diff with no diff") {
	Database db = some_database();
	Database db_2 = some_database();
	db_2.log = db.log;
	auto result = diff(db, db_2);

	CHECK(result.left.size() == 0);
	CHECK(result.right.size() == 0);
	CHECK(result.conflicts.size() == 0);
	CHECK(result.common.size() == db.log.size());

	for (int i = 0; i < db.log.size(); i++) {
		CHECK(result.common[i] == db.log[i]);
		CHECK(db.log[i] == db_2.log[i]);
	}
}

TEST_CASE("Test database diff left fast forward") {
	Database db = some_database();
	Database db_2 = some_database();
	db_2.log = db.log;

	auto extra_entries = some_positive_int(20);
	for (int i = 0; i < extra_entries; i++) {
		db.add_data_entry(some_entry());
	}

	auto result = diff(db, db_2);

	CHECK(result.left.size() == extra_entries);
	CHECK(result.right.size() == 0);
	CHECK(result.conflicts.size() == 0);
	CHECK(result.common.size() == db_2.log.size());

	for (int i = 0; i < db_2.log.size(); i++) {
		CHECK(result.common[i] == db.log[i]);
		CHECK(db.log[i] == db_2.log[i]);
	}
}

TEST_CASE("Test database diff right fast forward") {
	Database db = some_database();
	Database db_2 = some_database();
	db_2.log = db.log;

	auto extra_entries = some_positive_int(20);
	for (int i = 0; i < extra_entries; i++) {
		db_2.add_data_entry(some_entry());
	}

	auto result = diff(db, db_2);

	CHECK(result.left.size() == 0);
	CHECK(result.right.size() == extra_entries);
	CHECK(result.conflicts.size() == 0);
	CHECK(result.common.size() == db.log.size());

	for (int i = 0; i < db.log.size(); i++) {
		CHECK(result.common[i] == db.log[i]);
		CHECK(db.log[i] == db_2.log[i]);
	}
}

TEST_CASE("Test database diff both new no conflict") {
	Database db = some_database();
	Database db_2 = some_database();
	db_2.log = db.log;
	auto common_log = db.log;

	auto extra_entries = some_positive_int(20);
	for (int i = 0; i < extra_entries; i++) {
		db.add_data_entry(some_entry());
		db_2.add_data_entry(some_entry());
	}

	auto result = diff(db, db_2);

	CHECK(result.left.size() == extra_entries);
	CHECK(result.right.size() == extra_entries);
	CHECK(result.conflicts.size() == 0);
	CHECK(result.common.size() == common_log.size());

	for (int i = 0; i < common_log.size(); i++) {
		CHECK(result.common[i] == common_log[i]);
		CHECK(db.log[i] == common_log[i]);
		CHECK(db.log[i] == db_2.log[i]);
	}
}

TEST_CASE("Test database diff conflict") {
	Database db = some_database();
	Database db_2 = some_database();
	db_2.log = db.log;
	auto common_log = db.log;

	auto extra_entries = some_positive_int(20);
	auto conflicts = 0;
	for (int i = 0; i < extra_entries; i++) {
		auto entry = some_entry();
		auto conflicting_entry = some_entry();
		if (some_positive_int(4) <= 3) {
			conflicting_entry.name = entry.name;
			conflicts += 1;
		}

		db.add_data_entry(entry);
		db_2.add_data_entry(conflicting_entry);
	}

	auto result = diff(db, db_2);

	CHECK(result.left.size() == extra_entries);
	CHECK(result.right.size() == extra_entries);
	CHECK(result.conflicts.size() == conflicts);
	CHECK(result.common.size() == common_log.size());

	for (int i = 0; i < common_log.size(); i++) {
		CHECK(result.common[i] == common_log[i]);
		CHECK(db.log[i] == common_log[i]);
		CHECK(db.log[i] == db_2.log[i]);
	}
}


TEST_CASE("Test append_log to empty database") {
	Database db = some_database(false);

	db.append_log(vector<LogLine>());

	vector<LogLine> incoming_log;
	map<string, DataEntry> added_entries;
	string next_parent_id;
	for (int i = 0; i < 10 ; i++) {
		auto generated = some_entry();
		if (db.entries.find(generated.name) == db.entries.end()) {
			added_entries[generated.name] = generated;
		}
		incoming_log.push_back(LogLine(
			next_parent_id,
			LOG_ACTION::ADD,
			generated.name,
			generated
		));
		next_parent_id = incoming_log.back().id;
	}

	db.append_log(incoming_log);
	CHECK(db.log.size() == 10);
}


TEST_CASE("Test database append_log") {
	Database db = some_database();
	for (int i = 0; i < 20; i++) {
		db.add_data_entry(some_entry());
	}
	vector<string> existing_names;
	for (auto [entry_key, _]: db.entries) {
		existing_names.push_back(entry_key);
	}
	sort(
		existing_names.begin(),
		existing_names.end(),
		[](string a, string b){return rand() % 2 < 1;}
	);

	auto next_parent_id = db.log.back().id;
	vector<LogLine> incoming_logs;
	map<string, DataEntry> added_entries;
	for (int i = 0; i < 10 ; i++) {
		auto generated = some_entry();
		if (db.entries.find(generated.name) == db.entries.end()) {
			added_entries[generated.name] = generated;
		}
		incoming_logs.push_back(LogLine(
			next_parent_id,
			LOG_ACTION::ADD,
			generated.name,
			generated
		));
		next_parent_id = incoming_logs.back().id;
	}

	map<string, DataEntry> updated;
	for (int i = 0; i < 4 ; i++) {
		auto name = existing_names.back();
		auto generated = some_entry();
		generated.name = name;
		existing_names.pop_back();
		updated[name] = generated;
		incoming_logs.push_back(LogLine(
			next_parent_id,
			LOG_ACTION::UPDATE,
			generated.name,
			generated
		));
		next_parent_id = incoming_logs.back().id;
	}

	vector<string> deleted;
	for (int i = 0; i < 4 ; i++) {
		auto name = existing_names.back();
		existing_names.pop_back();
		deleted.push_back(name);
		incoming_logs.push_back(LogLine(
			next_parent_id,
			LOG_ACTION::REMOVE,
			name,
			db.entries[name]
		));
		next_parent_id = incoming_logs.back().id;
	}

	db.append_log(incoming_logs);

	for (auto [key, expected_entry]: added_entries) {
		CHECK(db.get_entry(key) == expected_entry);
	}

	for (auto [key, expected_entry]: updated) {
		CHECK(db.get_entry(key) == expected_entry);
	}

	for (auto key: deleted) {
		CHECK(db.entries.find(key) == db.entries.end());
	}
}
