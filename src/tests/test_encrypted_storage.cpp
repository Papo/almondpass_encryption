#include "../include/almond/include_test/catch.hpp"
#include "../include/almond/storage/encrypted_storage.hpp"
#include "../include/almond/encryption/encryption.hpp"
#include <string>
#include <cstdio>


TEST_CASE("Encrypted storage") {
	const char* tmp_file_name = "some_path.db";
	almond_storage::EncryptedStorage encrypted_storage(tmp_file_name);

	std::string salt(almond_enc::generate_key_salt());
	almond_enc::Key key = almond_enc::derive_key(
		std::string("some_key"),
		salt
	);
	std::string test_string("some_file_content");
	encrypted_storage.write(test_string, key);

	CHECK(encrypted_storage.read_key_salt() == salt);
	CHECK(encrypted_storage.read(key) == test_string);
	remove(tmp_file_name);
}
