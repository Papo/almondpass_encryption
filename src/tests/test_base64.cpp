#include "../include/almond/base64.hpp"

#include "./almond/include_test/catch.hpp"

using namespace almond_base_64;


TEST_CASE("base64 decode tests") {
    auto no_padding = decode(std::string("c29tZSB0ZXN0"));
    CHECK(no_padding == std::string("some test"));


    auto single_padding = decode(std::string("YSB0ZXN0IHN0cmluZwo="));
    CHECK(single_padding == std::string("a test string\n"));


    auto double_padding = decode(std::string("bGlnaHQgd29yaw=="));
    CHECK(double_padding == std::string("light work"));

    double_padding = decode(std::string("Yg=="));
    CHECK(double_padding == std::string("b"));

    CHECK_THROWS_AS(decode("bGlnaH=Qgd29yaw=="), Base64DecodeError);
    CHECK_THROWS_AS(decode("bGlnaH*&(Qgd29yaw=="), Base64DecodeError);
    CHECK_THROWS_AS(decode("bGlnaHQgd29yaw=1="), Base64DecodeError);
    CHECK_THROWS_AS(decode("bGlnaHQgd29yaw===="), Base64DecodeError);
    CHECK_THROWS_AS(decode("c29tZSB0ZXN"), Base64DecodeError);
    CHECK_THROWS_AS(decode("c="), Base64DecodeError);
    CHECK_THROWS_AS(decode("cc="), Base64DecodeError);
}


TEST_CASE("base64 encode test") {
    auto no_padding = encode(std::string("some test"));
    CHECK(no_padding == std::string("c29tZSB0ZXN0"));

    auto single_padding = encode(std::string("a test string\n"));
    CHECK(single_padding == std::string("YSB0ZXN0IHN0cmluZwo="));

    auto double_padding = encode(std::string("light work"));
    CHECK(double_padding == std::string("bGlnaHQgd29yaw=="));

    double_padding = encode(std::string("b"));
    CHECK(double_padding == std::string("Yg=="));
}
