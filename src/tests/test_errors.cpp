#include <string>

#include "../include/almond/errors.hpp"
#include "../include/almond/include_test/catch.hpp"

using namespace std;


TEST_CASE("Compile error produces correct output") {
	string message_template(
		"Some message template %s!"
	);

	string argument("template_argument");
	string compiled = compile_error_template(
		message_template.c_str(),
		"template_argument",
		"template_argument123"
	);

	CHECK(compiled == string("Some message template template_argument!"));
}
