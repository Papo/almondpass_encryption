#include "include/almond/errors.hpp"

#include <iostream>

std::string compile_error_template(const char* message_template, ...) {
	char message_buffer[MAX_ERROR_SIZE];
	va_list args;
	va_start(args, message_template);
	vsnprintf(
		message_buffer,
		MAX_ERROR_SIZE,
		message_template,
		args
	);
	va_end(args);
	return std::string(message_buffer);
}
