#include "../include/almond/data/data.hpp"

using namespace std;
using namespace chrono;
using namespace almond_data;


json11::Json almond_data::DataEntry::to_json() const {
	auto edited = almond_data::serialize(this->edited);

	return json11::Json::object {
		{"name", this->name},
		{"properties", this->properties},
		{"edited", edited}
	};
}

string almond_data::serialize(const LOG_ACTION action) {
	switch (action) {
	case LOG_ACTION::ADD:
		return string("ADD");
	case LOG_ACTION::REMOVE:
		return string("REMOVE");
	case LOG_ACTION::UPDATE:
		return string("UPDATE");
	}
}

almond_data::LOG_ACTION almond_data::parse_log_action(const string serialized) {
	if (serialized == string("ADD")) {
		return LOG_ACTION::ADD;
	}
	if (serialized == string("REMOVE")) {
		return LOG_ACTION::REMOVE;
	}
	if (serialized == string("UPDATE")) {
		return LOG_ACTION::UPDATE;
	}
}

almond_data::LogLine::LogLine(json11::Json parsed) {
	this->id = parsed["id"].string_value();
	this->parent_id = parsed["parent_id"].string_value();
	this->t = almond_data::parse_time(parsed["t"].string_value());
	this->action = almond_data::parse_log_action(
		parsed["action"].string_value()
	);
	this->entry_name = parsed["entry_name"].string_value();
	this->entry = DataEntry(parsed["entry"]);
}

json11::Json almond_data::LogLine::to_json() const {
	return json11::Json::object {
		{"id", this->id},
		{"parent_id", this->parent_id},
		{"t", serialize(this->t)},
		{"action", serialize(this->action)},
		{"entry_name", this->entry_name},
		{"entry", this->entry},
	};
}

almond_data::DataEntry::DataEntry(json11::Json parsed) {
	this->name = parsed["name"].string_value();
	this->edited = almond_data::parse_time(parsed["edited"].string_value());

	for (auto [key, value]: parsed["properties"].object_items()) {
		this->properties[key] = value.string_value();
	}
}


void almond_data::DataEntry::update_edited() {
	this->edited = system_clock::now();
}

void almond_data::DataEntry::add_property(string name) {
	this->add_property(name, string(""));
}


void almond_data::DataEntry::add_property(string name, string value) {
	if (this->properties.find(name) != this->properties.end()) {
		throw EntryAlreadyHasProperty(name, this->name);
	}

	this->properties[name] = value;
	this->update_edited();
}


void almond_data::DataEntry::set_property(string name, string value) {
	this->properties[name] = value;
	this->update_edited();
}


vector<string> almond_data::DataEntry::get_property_names() {
	vector<string> names;
	for (auto [name, _]: this->properties) {
		names.push_back(name);
	}

	return names;
}


string almond_data::DataEntry::get_property_value(string name) {
	if (this->properties.find(name) == this->properties.end()) {
		throw PropertyDoesNotExist(name, this->name);
	}

	return this->properties[name];
}


void almond_data::DataEntry::remove_property(string name) {
	if (this->properties.find(name) == this->properties.end()) {
		throw PropertyDoesNotExist(name, this->name);
	}

	this->properties.erase(name);
	this->update_edited();
}


string almond_data::Database::serialize() const {
	auto edited = almond_data::serialize(this->edited);

	json11::Json json_object = json11::Json::object {
		{"name", this->name},
		{"entries", this->entries},
		{"log", this->log},
		{"edited", edited}
	};
	return json_object.dump();
}


almond_data::Database almond_data::load(const string& serialized) {
	string error;
	json11::Json parsed = json11::Json::parse(serialized, error);

	if (error != "") {
		throw DatabaseLoadError();
	}

	map<string, DataEntry> entries;
	for (auto [entry_key, entry]: parsed["entries"].object_items()) {
		entries[entry_key] = DataEntry(entry);
	}

	string name = parsed["name"].string_value();
	time_point<system_clock> edited = almond_data::parse_time(
		parsed["edited"].string_value()
	);

	vector<LogLine> _logs;
	for (auto log_line: parsed["log"].array_items()) {
		_logs.push_back(LogLine(log_line));
	}
	 Database db(name, edited, entries);
	 db.log = _logs;
	 return db;
}


vector<string> almond_data::Database::get_entry_keys() {
	vector<string> keys;
	for (auto [entry_key, _]: this->entries) {
		keys.push_back(entry_key);
	}

	return keys;
}


void almond_data::Database::add_data_entry(DataEntry entry) {
	if (this->entries.find(entry.name) != this->entries.end()) {
		throw EntryAlreadyExists(entry.name, this->name);
	}

	auto parent_id = this->log.empty() ? string(): this->log.back().id;
	LogLine log_entry(
		parent_id, LOG_ACTION::ADD, entry.name, entry
	);
	this->log.push_back(log_entry);
	this->entries[entry.name] = entry;
	this->update_edited();
}


void almond_data::Database::update_entry(
	almond_data::DataEntry const updated
) {
        if (this->entries.find(updated.name) == this->entries.end()) {
            throw EntryDoesNotExist(updated.name, this->name);
        }

		auto parent_id = this->log.empty() ? string(): this->log.back().id;
		this->log.push_back(LogLine(
			parent_id,
			LOG_ACTION::UPDATE,
			updated.name,
			updated
		));
        this->entries[updated.name] = updated;
        this->update_edited();
}


almond_data::DataEntry almond_data::Database::get_entry(string entry_name) {
	if (this->entries.find(entry_name) == this->entries.end()) {
		throw EntryDoesNotExist(entry_name, this->name);
	}

	return this->entries[entry_name];
}

vector<string> almond_data::Database::search_name(string name) {
	vector<string> matches;
	for (auto entry: this->get_entry_keys()) {
		if (entry.substr(0, name.length()) == name) {
			matches.push_back(entry);
		}
	}

	return matches;
}

void almond_data::Database::remove_data_entry(string name) {

	auto iter = this->entries.find(name);
	if (iter == this->entries.end()) {
		throw EntryDoesNotExist(name, this->name);
	}
	auto entry = iter->second;
	auto parent_id = this->log.empty() ? string(): this->log.back().id;
	this->log.push_back(LogLine(parent_id, LOG_ACTION::REMOVE, name, entry));
	this->entries.erase(name);
	this->update_edited();
}


void almond_data::Database::update_edited() {
	this->edited = system_clock::now();
}


void almond_data::Database::write(
	almond_storage::EncryptedStorage storage, almond_enc::Key key
) {
	auto _data = this->serialize();
	storage.write(_data, key);
	this->saved = system_clock::now();
}


almond_data::Database almond_data::read(
	almond_storage::EncryptedStorage storage, almond_enc::Key key
) {
	auto _data = storage.read(key);
	return almond_data::load(_data);
}


time_point<system_clock> almond_data::Database::get_last_edited() {
	time_point last_edited = this->edited;

	for (auto [_, entries]: this->entries) {
		if (entries.edited > last_edited) {
			last_edited = entries.edited;
		}
	}

	return last_edited;
}


void almond_data::Database::append_log(vector<LogLine> log) {
	for (auto line: log) {
		if (!this->log.empty()) {
			auto last_id = this->log.back().id;
			this->log.push_back(line);
			this->log[this->log.size() - 1].parent_id = last_id;
		} else {
			this->log.push_back(line);
		}
	}
	this->collapse_log();
}


bool almond_data::Database::is_unsaved() {
	auto edited = this->get_last_edited();
	return edited > this->instantiated && edited > this->saved;
}


bool almond_data::operator==(const Database& lho, const Database& rho) {
	if (lho.name != rho.name) {
		return false;
	}

	if (lho.log != rho.log) {
		return false;
	}

	return lho.entries == rho.entries;
}


bool almond_data::operator!=(const Database& lho, const Database& rho) {
	return !(lho == rho);
}


bool almond_data::operator==(const DataEntry& lho, const DataEntry& rho) {
	if (lho.name != rho.name) {
		return false;
	}

	return lho.properties == rho.properties;
}


bool almond_data::operator!=(const DataEntry& lho, const DataEntry& rho) {
	return !(lho == rho);
}

void almond_data::Database::collapse_log() {
	this->entries = map<string, DataEntry>();
	for (auto log = this->log.begin(); log != this->log.end(); log++) {
		switch (log->action){
			case LOG_ACTION::ADD:
				this->entries[log->entry_name] = log->entry;
				break;
			case LOG_ACTION::UPDATE:
				this->entries[log->entry_name] = log->entry;
				break;
			case LOG_ACTION::REMOVE:
				if (this->entries.find(log->entry_name) != this->entries.end()) {
					this->entries.erase(log->entry_name);
				}
				break;
		}
	}

	if (!this->log.empty()) {
		this->edited = this->log.back().t;
	}
}


Diff almond_data::diff(const Database& lho, const Database& rho) {
	Diff diff;
	map<string, Conflict> conflicts;

	int last_common_index = 0;
	for (int i=0; i < lho.log.size(); i++) {
		auto log_line = lho.log[i];
		if (i < rho.log.size() && log_line == rho.log[i]) {
			diff.common.push_back(log_line);
			last_common_index++;
			continue;
		}

		diff.left.push_back(log_line);

		auto entry_logs = conflicts.find(log_line.entry_name);
		if (entry_logs == conflicts.end()) {
			conflicts[log_line.entry_name] = Conflict();
		}

		conflicts[log_line.entry_name].left.push_back(log_line);
	}

	for (int i=last_common_index; i < rho.log.size(); i++) {
		auto log_line = rho.log[i];
		diff.right.push_back(rho.log[i]);

		auto result = conflicts.find(log_line.entry_name);
		if (result == conflicts.end()) {
			conflicts[log_line.entry_name] = Conflict();
			continue;
		}

		conflicts[log_line.entry_name].right.push_back(log_line);
	}

	for (auto [_, conflict]: conflicts) {
		if (!conflict.left.empty() && !conflict.right.empty()) {
			diff.conflicts.push_back(conflict);
		}
	}

	return diff;
}


bool almond_data::operator==(const LogLine& lho, const LogLine& rho) {
	if (lho.id != rho.id || lho.parent_id != rho.parent_id) {
		return false;
	}

	if (lho.action != rho.action) {
		return false;
	}

	if (lho.entry_name != rho.entry_name) {
		return false;
	}

	if (lho.entry != rho.entry) {
		return false;
	}

	return true;
}


bool almond_data::operator!=(const LogLine& lho, const LogLine& rho) {
	return !(lho == rho);
}
