#include "../include/almond/data/utils.hpp"

using namespace almond_data;

std::string almond_data::_strftime(std::chrono::time_point<std::chrono::system_clock> time_, const char* fmt) {
	auto parsed = std::chrono::system_clock::to_time_t(time_);
	auto serialized_saved = std::stringstream();
	serialized_saved << std::put_time(std::gmtime(&parsed), fmt);

	return serialized_saved.str();
}


std::string almond_data::serialize(std::chrono::time_point<std::chrono::system_clock> time_) {
	std::stringstream serialized;
	serialized << std::chrono::duration_cast<std::chrono::microseconds>(
		time_.time_since_epoch()
	).count();

	return serialized.str();
}


std::chrono::time_point<std::chrono::system_clock> almond_data::parse_time(std::string seconds_since_epoch) {
	std::stringstream serialized(seconds_since_epoch);
	serialized.seekg(serialized.beg);

	int64_t parsed = 0;
	serialized >> parsed;

	std::chrono::time_point<std::chrono::system_clock> tp;
	return tp + std::chrono::microseconds(parsed);
}
