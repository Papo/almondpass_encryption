#include <fstream>
#include <filesystem>
#include <cstdio>
#ifdef _WIN32
#include <windows.h>
#endif

#include "../include/almond/storage/local.hpp"


almond_storage::LocalStorage::LocalStorage(const char* path_param)
: path(path_param) {}

almond_storage::LocalStorage::LocalStorage(std::string path_param)
: path(path_param) {}


std::string almond_storage::LocalStorage::get_temp_path() {
	return this->path + ".tmp";
}

std::string almond_storage::LocalStorage::read_file_version() {
	std::string result;
	std::ifstream input_file(this->path, std::ifstream::binary);

	char buffer[FILEFORMAT_VERSION_WIDTH];
	input_file.read(buffer, FILEFORMAT_VERSION_WIDTH);
	int bytes_read = input_file.gcount();
	result.append(buffer, bytes_read);
	input_file.close();

	return result;
}

std::string almond_storage::LocalStorage::read(int offset) {
	std::string result;
	std::ifstream input_file(this->path, std::ifstream::binary);

	// skip file version and other prefixes
	input_file.seekg(FILEFORMAT_VERSION_WIDTH + offset);

	const int buffer_size = 2 * 1024;
	char buffer[buffer_size];
	while(!input_file.eof()) {
		input_file.read(buffer, buffer_size);
		int bytes_read = input_file.gcount();
		result.append(buffer, bytes_read);
	}
	input_file.close();

	return result;
}

void almond_storage::LocalStorage::write(std::string buffer) {
	#ifdef _WIN32
	std::string write_path = this->path;
	std::ios_base::openmode mode = std::ios_base::trunc | std::ios_base::binary;
	#else
	std::string write_path = this->get_temp_path();
	std::ios_base::openmode mode = std::ios_base::out;
	#endif
	auto parent_path = std::filesystem::path(write_path).parent_path();

	if (parent_path != "") {
		std::filesystem::create_directories(parent_path);
	}

	std::ofstream output_file(write_path, mode);

	output_file.write(FILEFORMAT_VERSION, FILEFORMAT_VERSION_WIDTH);
	output_file.write(buffer.data(), buffer.size());
	output_file.flush();
	output_file.close();

	#ifndef _WIN32
	rename(write_path.c_str(), this->path.c_str());
	#endif

}
