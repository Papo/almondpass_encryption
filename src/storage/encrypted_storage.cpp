#include "../include/almond/storage/encrypted_storage.hpp"
#include <fstream>
#include <cstdio>


almond_storage::EncryptedStorage::EncryptedStorage(const char* path)
: storage(path) {}


almond_storage::EncryptedStorage::EncryptedStorage(std::string path)
: storage(path) {}


std::string almond_storage::EncryptedStorage::read_key_salt() {
	std::string result;
	std::ifstream input_file(this->storage.path, std::ifstream::binary);

	char buffer[crypto_pwhash_SALTBYTES];
	input_file.seekg(FILEFORMAT_VERSION_WIDTH);
	input_file.read(buffer, crypto_pwhash_SALTBYTES);
	int bytes_read = input_file.gcount();
	result.append(buffer, bytes_read);
	input_file.close();

	return result;
}

std::string almond_storage::EncryptedStorage::read(almond_enc::Key key) {
	return almond_enc::decrypt_message(this->read(), key);
}

almond_enc::EncryptedMessage almond_storage::EncryptedStorage::read() {
	std::string encrypted = this->storage.read(crypto_pwhash_SALTBYTES);
	return almond_enc::message_from_string(encrypted);
}

void almond_storage::EncryptedStorage::write(
	std::string text, almond_enc::Key key
){
	this->write(almond_enc::encrypt_message(text, key), key.salt);
}

void almond_storage::EncryptedStorage::write(
	almond_enc::EncryptedMessage message,
	std::string key_salt
) {
	std::string encrypted = almond_enc::message_to_string(message);
	this->storage.write(key_salt + encrypted);
}
