#!/usr/bin/env python3


import os, urllib.request, shutil, subprocess
import ssl
from glob import glob
import certifi

VERSION="1.7.1"
MAJOR_MINOR_VERSION=".".join(VERSION.split(".")[:1])

with open("meson_options.txt", "w") as meson_options:
    meson_options.write(
        "option('version', type: 'string', value: '%s')\n" % MAJOR_MINOR_VERSION,
    )
    meson_options.write(
        "option('soversion', type : 'string', value : '%s')\n" % VERSION
    )

sodium_url = (
    'https://download.libsodium.org/libsodium/releases/'
    'libsodium-1.0.18-msvc.zip'
)
sodium_filename = sodium_url.split('/')[-1]
sodium_dir = sodium_filename.split('-')[0]
ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS)
ssl_context.verify_mode = ssl.CERT_REQUIRED
ssl_context.check_hostname = True
ssl_context.load_default_certs()
ssl_context.load_verify_locations(
    cafile=os.path.relpath(certifi.where()),
    capath=None,
    cadata=None)

if not os.path.exists(sodium_filename):
    response = urllib.request.urlopen(sodium_url, timeout=600.0, context=ssl_context)
    data = response.read()
    with open(sodium_filename, 'wb') as sodium_file:
        sodium_file.write(data)

shutil.rmtree('build', ignore_errors=True)
os.mkdir('build')
os.mkdir('build/lib')
shutil.unpack_archive(sodium_filename, 'build')
shutil.copy(
    os.path.join('build', sodium_dir, 'x64/Release/v142/dynamic/libsodium.lib'),
    'build/lib'
)
shutil.copy(
    os.path.join('build', sodium_dir, 'x64/Release/v142/dynamic/libsodium.dll'),
    'build/lib'
)

subprocess.check_call(
    ['meson', 'setup', '--buildtype', 'release', 'build']
)
subprocess.check_call(
    ['meson', 'compile', '-C', 'build']
)

package_dir = 'build/package'
include_dir = os.path.join(package_dir, "include")
shutil.copytree(
    'src/include',
    include_dir
)
shutil.copy(
    'build/libalmond.a',
    package_dir,
)

shutil.make_archive(
    'almond_encryption-%s-msvc' % VERSION,
    'zip',
    package_dir
)
