[binaries]
cpp = '/build/sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/armv7a-linux-androideabi30-clang++'
ar = '/build/sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/arm-linux-androideabi-ar'
as = '/build/sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/arm-linux-androideabi-as'
strip = '/build/sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/arm-linux-androideabi-strip'
ranlib = '/build/sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/arm-linux-androideabi-ranlib'
ld = '/build/sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/arm-linux-androideabi-ld.bfd'

pkgconfig = 'pkg-config'

[built-in options]
cpp_args = '-target armv7a-linux-android'

[properties]
pkg_config_libdir = ['/build/pkg_config_dirs/armv7a-linux-android']

[host_machine]
system = 'linux'
cpu_family = 'armv7a'
cpu = 'armv7a'
endian = 'little'
