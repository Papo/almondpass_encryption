#!/bin/sh
MESON_ACTION=${1:-'compile'}

VERSION=1.7.1
SOVERSION="$(echo $VERSION | cut -f 1-2 -d '.')"

printf "option('version', type : 'string', value : '%s')\noption('soversion', type : 'string', value : '%s')\n" "$VERSION" "$SOVERSION" > meson_options.txt

docker build -t almond_encryption .
docker run \
    -v "$(pwd)/docker_build":/build_dir/build \
    -v "$(pwd)/src":/build_dir/src \
    --env MESON_ACTION="$MESON_ACTION" \
    --env DESTDIR='/build_dir/build/dist' \
    --env VERSION="$VERSION" \
    --env SOVERSION="$SOVERSION" \
    almond_encryption
