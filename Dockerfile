FROM debian:bullseye-slim
ENV MESON_ACTION compile

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y clang meson pkg-config apt-utils
RUN apt-get install -y libsodium23 libsodium-dev

WORKDIR /encryption
COPY build_scripts/create_build.sh meson.build /encryption/
COPY src /encryption/src
COPY LICENCE /encryption/LICENCE
COPY meson_options.txt meson_options.txt

ENTRYPOINT ./create_build.sh
