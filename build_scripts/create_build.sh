#!/bin/sh
CC=clang meson build -Db_lundef=false

cd build || exit

DESTDIR=$(printf "/build_target/libalmond%s_%s-1_amd64" "$SOVERSION" "$VERSION")

mkdir -p "$DESTDIR"

if [ "$MESON_ACTION" = "install" ]; then
    # Install project in DESTDIR
    meson configure --prefix "/usr"
    meson configure --strip
    LDFLAGS=-static-libstdc++ meson "$MESON_ACTION"

    # Prepare binary package
    DEBIAN_DIR="$DESTDIR/DEBIAN"
    mkdir -p "$DEBIAN_DIR"

    printf "Package: libalmond%s\n" "$SOVERSION"> "$DEBIAN_DIR/control"
    {
        printf "Version: %s\n" "$VERSION"
        printf "Architecture: amd64\n"
        printf "Maintainer: %s\n" "maintainer@almondpass.com"
        printf "Description: %s\n" "Almondpass encryption module"
        printf "Depends: %s, libc6 (>= 2.25)\n" "libsodium23 (>= 1.0)"
        printf "Provides: libalmond%s (= %s)\n" "$SOVERSION" "$VERSION"
    } >> "$DEBIAN_DIR/control"

    printf "activate-noawait ldconfig\n" > "$DEBIAN_DIR/triggers"
    printf "libalmond %s\n" "$SOVERSION" > "$DEBIAN_DIR/shlibs"

    dpkg --build "$DESTDIR"
    mv "$DESTDIR.deb" /build_dir/build
else
    meson "$MESON_ACTION"
    cp -r /encryption/build/meson-logs /build_dir/build
fi
