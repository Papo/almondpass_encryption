#!/bin/sh
for ARCH in "aarch64" "armv7a" "i686" "x86_64"
do
    cd /build/ || exit
    BUILDDIR="$ARCH"_build
    meson "$BUILDDIR" --cross-file crossfiles/"$ARCH"_linux_android.txt
    cd "$BUILDDIR" || exit

    LIBNAME=$(printf "libalmond%s_%s-1_%s_linux_android" "$SOVERSION" "$VERSION" "$ARCH")
    DESTDIR=$(printf "/build/target/%s" "$LIBNAME")
    export LIBNAME
    export DESTDIR
    if [ "$MESON_ACTION" = "install" ]; then
        mkdir -p "$DESTDIR"
        meson configure --prefix "/build/"
        meson "$MESON_ACTION"
        cd "/build/target/" || exit
        tar -cf "$ARCH"_linux_android.tar "$LIBNAME"
    else
        meson "$MESON_ACTION"
    fi
done
