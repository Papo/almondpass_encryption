Almond Crypto
=============

A secret storage library.

Building
-------

To create the build directory run `./create_build.sh`.

To build run `ninja` in the build directory.

Testing
------

To build and run tests run `ninja tests`
