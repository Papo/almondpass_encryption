#!/bin/sh
MESON_ACTION=${1:-'compile'}
CC=clang meson build -Db_lundef=false

cd build || exit

DESTDIR=$(printf "./libalmond%s" "$VERSION")

rm -r "$DESTDIR"
mkdir -p "$DESTDIR" || exit


if [ "$MESON_ACTION" = "install" ]; then
  rm -r "$DESTDIR".zip
  # meson configure --strip
  meson configure --prefix "/"
  LDFLAGS=-static-libstdc++ meson "$MESON_ACTION" --destdir "$DESTDIR"

  zip -0r "$DESTDIR".zip "$DESTDIR"
  rm -r ../../imgui_client/libalmond
  rm ../../imgui_client/libalmond.zip
  cp "$DESTDIR".zip ../../imgui_client
  cd ../../imgui_client
  unzip libalmond.zip
else
  meson "$MESON_ACTION"
fi
